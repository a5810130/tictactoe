import random
class Model:

    def __init__(self):                            # Constructure for buid object
        self.table = [[' ',' ',' '],[' ',' ',' '],[' ',' ',' ']]    # List size 3x3 for storage X & O from input
    
    
    def get_data(self,x,y):
        return self.table[x][y]
    
    def get_tableSize(self):
        return len(self.table)
    
    def set_table(self,x,y,symbol):
        self.table[x][y] = symbol

    def tableError(self,x,y):                                                           
        if( self.table[x][y] != ' '):                                                   # Check if select point isn't empty
            return True
        else:
            return False
        
    def checkWin(self):                                    # Check the current player turn win or not
        for x in range(3):                                
            if(self.table[x][0] == self.table[x][1] == self.table[x][2] != " "):     # Check if player win by one in three row 
                return True                                 # If true return current player win
            elif(self.table[0][x] == self.table[1][x] == self.table[2][x] != " "):    # Check if player win by one in three collum
                return True                                 # If true return current player win
            elif(self.table[0][0] == self.table[1][1] == self.table[2][2] != " "):    # Check if player win by |\| this pattern
                return True                                 # If true return current player win
            elif(self.table[2][0] == self.table[1][1] == self.table[0][2] != " "):    # Check if player win by |/| this pattern
                return True                                 # If true return cuurent player win 
        else:                                        # If none of this is true
            return False                                # Return false nobody win keep playing or draw

    def checkDraw(self):                                # Check draw
        blank = 0
        for x in range(3):
            for y in range(3):                                # Check if any value in list = " " then keep playing 
                if self.table[x][y] == " ":                        # If not then blank = 0
                    blank +=1
        if blank == 0:                                    # When blank = 0 then it means draws
            return True
        else: return False
        
class Controller:
    
    def __init__(self,m,v):
        self.m = m
        self.v = v
        turnNumber = random.randint(1,2)
        while(True):                                    # Infinite loop for checking
            self.turn(turnNumber)
            if(self.m.checkWin()):                                # Call method check win
                self.v.show_table()
                self.v.show_win(turnNumber)                                # Tell that O win
                exit()                                    # Have a winner exit and close program
            if(self.m.checkDraw()):
                self.v.show_table()
                self.v.show_draw()
                exit()
            turnNumber += 1
                    

    def turn(self,turnNumber):                                # Tell who's turn O or X and count turn
        self.v.show_table()
        self.v.show_turn(turnNumber)  
        n = input("Choose Position ('n' or 'x y'): ")
        if(self.checkInput(n)):
            self.v.show_error()
            self.turn(turnNumber)
        else:
            if (len(n) == 1):
                self.add(turnNumber,n)
            else:
                self.add(turnNumber,n[0],n[2])

    def checkInput(self,n):
        if (len(n) == 1):
            if (ord(n) < 49 or ord(n) > 57 ):
                return True
            else: return False
        elif (len(n) == 3 and n[1] == ' '):
            if (ord(n[0]) < 48 or ord(n[0]) > 50  or ord(n[2]) < 48 or ord(n[2]) > 50):
                return True
            else: return False
        else:
            return True

    
    def add(self,turnNumber,point_1,point_2=None):
        if(point_2 == None):
            x = (int(point_1)-1)//3
            y = (int(point_1)-1)%3
        else:
            x = int(point_2)
            y = int(point_1)
        if (self.m.tableError(x,y)):
            self.v.show_error()
            self.turn(turnNumber)
        else:
            if turnNumber % 2 == 0:
                self.m.set_table(x,y,'X')                    # put X in specific row and collum recieved value from argument x & y
            else :
                self.m.set_table(x,y,'O')                    # put O in specific row and collum recieved value from argument x & y

    
class Viewer:
    def __init__(self,m):
        self.m = m
    def show_table(self):                        # Draw table and show what is inside in each collum and row
        num = 1
        for i in range(0,self.m.get_tableSize()):                
            for j in range(0,self.m.get_tableSize()):
                print("|"+self.m.get_data(i,j) ,end= "|")
            print("   " ,end="")
            print("|"+str(num)+"|"+"|"+str(num+1)+"|"+"|"+str(num+2)+"|" ,end= "")
            num += 3
            print()
    def show_error(self):
        print("\nERROR!!! : Try Again\n")
    def show_win(self,turnNumber):
        if turnNumber % 2 == 0:                            # Check who's turn is win X or O
            print("X Win")                                # Tell that X win
        else :
            print("O Win")
    def show_draw(self):
        print("DRAW")
        
    def show_turn(self,turnNumber):
        if turnNumber % 2 == 0:                                # If true it mean X turn if not Y turn 
            print("X turn")
        else :                                        
            print("O turn")
        
def main():
    m = Model()
    v = Viewer(m)
    c = Controller(m,v)
    

    
    
main()
import random
class Model:

    def __init__(self):							# Constructure for buid object
        self.table = [[' ',' ',' '],[' ',' ',' '],[' ',' ',' ']]	# List size 3x3 for storage X & O from input

    def get_table(self):
        return self.table
    
    def set_table(self,x,y,symbol):
        self.table[y][x] = symbol

    def tableError(self,x,y):                                                           
        if( self.table[y][x] != ' '):                                                   # Check if select point isn't empty
            return True
        else:
            return False
        
    def checkWin(self):									# Check the current player turn win or not
        for x in range(3):								
            if(self.table[x][0] == self.table[x][1] == self.table[x][2] != " "): 	# Check if player win by one in three row 
                return True							 	# If true return current player win
            elif(self.table[0][x] == self.table[1][x] == self.table[2][x] != " "):	# Check if player win by one in three collum
                return True							 	# If true return current player win
            elif(self.table[0][0] == self.table[1][1] == self.table[2][2] != " "):	# Check if player win by |\| this pattern
                return True							 	# If true return current player win
            elif(self.table[2][0] == self.table[1][1] == self.table[0][2] != " "):	# Check if player win by |/| this pattern
                return True							 	# If true return cuurent player win 
        else:										# If none of this is true
            return False								# Return false nobody win keep playing or draw

    def checkDraw(self):								# Check draw
        blank = 0
        for x in range(3):
            for y in range(3):								# Check if any value in list = " " then keep playing 
                if self.table[x][y] == " ":						# If not then blank = 0
                    blank +=1
        if blank == 0:									# When blank = 0 then it means draws
            return True
        else: return False
        
class Controller:
    
    def __init__(self,m,v):
        self.m = m
        self.v = v		

    def turn(self,turnCounter):								# Tell who's turn O or X and count turn
        self.turnCounter = turnCounter
        self.v.show_table()
        self.v.show_turn(self.turnCounter)
        self.input()
        self.add()
        if(self.m.checkWin()):								# Call method check win
            self.v.show_table()
            self.v.show_win(turnCounter)								# Tell that O win
            exit()									# Have a winner exit and close program
        if(self.m.checkDraw()):
            self.v.show_table()
            self.v.show_draw()
            exit()
   
    def add(self):
        if (self.m.tableError(self.x,self.y)):
            self.v.show_error()
            self.turn(self.turnCounter)
        else:
            if self.turnCounter % 2 == 0:
                self.m.set_table(self.x,self.y,'X')					# put X in specific row and collum recieved value from argument x & y
            else :
                self.m.set_table(self.x,self.y,'O')					# put O in specific row and collum recieved value from argument x & y

class ControllerType1(Controller):
    def input(self):
        n = input("Choose Position (1-9) :")
        if(self.checkInput(n) == False):
            self.v.show_error()
            self.input()
        else:
            self.x = (int(n)-1)%3
            self.y = (int(n)-1)//3
        

    def checkInput(self,n):
        if (len(n) == 1 and ord(n) >= 49 and ord(n) <= 57 ):
            return True
        else: return False

        
class ControllerType2(Controller):
    def input(self):
        x = input("Choose column (1-3) :")
        y = input("Choose row (1-3) :")
        if(self.checkInput(x) == False or self.checkInput(y) == False):
            self.v.show_error()
            self.input()
        else:
            self.x = int(x)-1
            self.y = int(y)-1
        

    def checkInput(self,n):
        if (len(n) == 1 and ord(n) >= 49 and ord(n) <= 51 ):
            return True
        else: return False
        
class Viewer:
    def __init__(self,m):
        self.m = m
    def show_table(self):						# Draw table and show what is inside in each collum and row
        num = 1
        for i in range(0,len(self.m.get_table())):				
            for j in range(0,len(self.m.get_table()[i])):
                print("|"+self.m.get_table()[i][j] ,end= "|")
            print("   " ,end="")
            print("|"+str(num)+"|"+"|"+str(num+1)+"|"+"|"+str(num+2)+"|" ,end= "")
            num += 3
            print()
    def show_error(self):
        print("\nERROR!!! : Try Again\n")
    def show_win(self,turnCounter):
        if turnCounter % 2 == 0:							# Check who's turn is win X or O
            print("X Win")								# Tell that X win
        else :
            print("O Win")
    def show_draw(self):
        print("DRAW")
        
    def show_turn(self,turnCounter):
        if turnCounter % 2 == 0:								# If true it mean X turn if not Y turn 
            print("X turn")
        else :										
            print("O turn")
        
def main():
    m = Model()
    v = Viewer(m)
    mode = input("select mode (1,2)?")
    if(mode == '1'):
        c = ControllerType1(m,v)
    elif(mode == '2'):
        c = ControllerType2(m,v)                                                                 
    turnCounter = random.randint(1,2)
    while(True):									# Infinite loop for checking
        c.turn(turnCounter)
        turnCounter += 1


    
    
main()
 

class Tictactoe:							# Class for play game Tictactoe
    def __init__(self):							# Constructure for buid object
        #self.x = x
        #self.y = y
        self.table = [[' ',' ',' '],[' ',' ',' '],[' ',' ',' ']]	# List size 3x3 for storage X & O from input

    def add_x(self,x,y):						# Method that put X in table with specific loaction row and collum
        self.table[x][y] = 'X'						# put X in specific row and collum recieved value from argument x & y
        self.show_table()						# Then after put X in table then show what it looks like

    def add_o(self,x,y):						# Method that put X in table with specific loaction row and collum 
        self.table[x][y] = 'O'						# put O in specific row and collum recieved value from argument x & y
        self.show_table()						# Then after put O in table then show what it looks like

    def show_table(self):						# Draw table and show what is inside in each collum and row
        for i in range(0,len(self.table)):				
            for j in range(0,len(self.table[i])):
                print("|"+self.table[i][j] ,end= "|")
            print()

    def checkerror(self,x,y):						# Check if input are only 0, 1, 2 if not then re-input
        if(int(x) > 2 or int(y) > 2 or x == None or y == None  or self.table[int(x)][int(y)] != " " ):
            return True
        else:
            return False

    def checkWin(self):									# Check the current player turn win or not
        for x in range(3):								# Make variable x have 3 value is 0, 1, 2
            if(self.table[x][0] == self.table[x][1] == self.table[x][2] != " "): 	# Check if player win by one in three row 
                return True							 	# If true return current player win
            elif(self.table[0][x] == self.table[1][x] == self.table[2][x] != " "):	# Check if player win by one in three collum
                return True							 	# If true return current player win
            elif(self.table[0][0] == self.table[1][1] == self.table[2][2] != " "):	# Check if player win by |\| this pattern
                return True							 	# If true return current player win
            elif(self.table[2][0] == self.table[1][1] == self.table[0][2] != " "):	# Check if player win by |/| this pattern
                return True							 	# If true return cuurent player win 
        else:										# If none of this is true
            return False								# Return false nobody win keep playing or draw
    def checkDraw(self):								# Check draw
        blank = 0
        for x in range(3):
            for y in range(3):								# Check if any value in list = " " then keep playing 
                if self.table[x][y] == " ":						# If not then blank = 0
                    blank +=1
        if blank == 0:									# When blank = 0 then it means draws
            print("Draw")								# Let the player know that this turn is draw
            exit()									# Exit the game program close
                    
def turn(ox,turnCounter):								# Tell who's turn O or X and count turn
    if turnCounter % 2 == 0:								# If true it mean X turn if not Y turn 
        print("X turn")
    else :										
        print("O turn")
        
    y = input("which row from 0 - 2 ? (Left to right) ")				# Interface ask player which row 
    x = input("which column from 0 - 2? (Up to Down) ")					# Interface ask player which collum

    if(ox.checkerror(x,y)):								# Check error for wrong input e.g. 3 or four
        turn(ox, turnCounter)
    else:										# If not error then add X or O from x,y input
        if turnCounter % 2 == 0:
            ox.add_x(int(x),int(y))
        else :
            ox.add_o(int(x),int(y))
        

def main():										# Function main
    ox = Tictactoe()									# Make ox to be Tictactoe object
    ox.show_table()									# Call method show_table() to show table of ox
    turnNumber = 1									# Keep value of turn number by start at 1
    while(True):									# Infinite loop for checking
        turn(ox,turnNumber)
        if(ox.checkWin()):								# Call method check win
            if turnNumber % 2 == 0:							# Check who's turn is win X or O
                print("X Win")								# Tell that X win
            else :
                print("O Win")								# Tell that O win
            exit()									# Have a winner exit and close program
        ox.checkDraw()
        turnNumber += 1									# Plus turn number for count that who's turn 


main()											# Call function main
